#[tokio::main]
async fn main() {
    let tika = tikars::Tika::from_url("http://localhost:9998/tika", Some(String::from("fra+eng")));

    let result = tika.get_text("files/lorem.png");

    assert!(result.is_ok(), "Got error, check that tika server is running");
    let parsed_text = result.unwrap();

    let ref_text = std::fs::read("files/lorem.txt").unwrap();
    assert_eq!(ref_text, parsed_text.as_bytes().to_vec());

    println!("Everything runs well !")
}
