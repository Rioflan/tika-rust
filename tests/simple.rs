mod tests {

    #[test]
    fn get_text() {
        let tika_url =
            std::env::var("TEST_TIKA_URL").unwrap_or_else(|_| String::from("http://127.0.0.1:9998/tika"));
        let tika =
            tikars::Tika::from_url(&tika_url, Some(String::from("fra+eng")));

        match tika.get_text("files/lorem.png") {
            Ok(result) => {
                let ref_text = std::fs::read("files/lorem.txt").unwrap();
                assert_eq!(ref_text, result.as_bytes().to_vec(), "Does the file exists ?");
            }
            Err(err) => {
                panic!("Got error on lib: {}", err);
            }
        }
    }
}
