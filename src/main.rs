#![warn(clippy::semicolon_if_nothing_returned)]
extern crate tokio;

use std::env;
use tikars::Tika;

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();

    let tika_url =
        env::var("TIKA_URL").unwrap_or_else(|_| String::from("http://localhost:9998/tika"));
    dbg!(&tika_url);
    let tika = Tika::from_url(&tika_url, Some(String::from("fra+eng")));
    std::process::exit(match tika.get_text(&args[1]) {
        Ok(result) => {
            print!("{}", result);
            0
        },
        Err(err) => {
            eprintln!("Got error on lib: {}", err);
            1
        }
    });
}
