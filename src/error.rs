#[derive(Debug)]
pub enum Error {
    Io(std::io::Error),
    Reqwest(reqwest::Error),
    #[allow(dead_code)]
    Unexpected(String),
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Error {
        Error::Reqwest(err)
    }
}

impl std::error::Error for Error {
    fn cause(&self) -> Option<&dyn std::error::Error> {
        match self {
            Error::Io(ref err) => Some(err),
            Error::Reqwest(ref err) => Some(err),
            Error::Unexpected(_) => None,
        }
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::Io(ref err) => write!(f, "std::io::Error: {}", err),
            Error::Reqwest(err) => write!(f, "reqwest::Error: {}", err),
            Error::Unexpected(message) => write!(f, "Unxepected error: {}", message),
        }
    }
}
