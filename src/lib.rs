//! Tika Rust
#![deny(clippy::all, clippy::pedantic, missing_debug_implementations, missing_docs, unsafe_code)]
extern crate mime;
extern crate reqwest;
mod error;
use error::Error;
use reqwest::header::{HeaderMap, HeaderValue, ACCEPT, CONTENT_TYPE};

/// `Tika` is the main struct of the library
#[derive(Debug)]
pub struct Tika {
    url: String,
    languages: Option<String>,
}

impl Tika {
    /// Default constructor for `Tika`
    #[must_use]
    pub fn default() -> Self {
        Self { url: String::from("http://localhost:9998/tika"), languages: None }
    }

    /// `Tika`'s constructor `from_url` allows to specify custom url for Tika
    #[must_use]
    pub fn from_url(url: &str, languages: Option<String>) -> Self {
        Self { url: url.to_owned(), languages }
    }

    fn get_headers(&self, file_path: &str) -> HeaderMap {
        let mut headers = HeaderMap::new();
        let file_mime = mime_guess::from_path(file_path).first_raw().unwrap();
        headers.insert(ACCEPT, HeaderValue::from_static("text/plain"));
        headers.insert(CONTENT_TYPE, file_mime.parse().unwrap());
        if let Some(languages) = &self.languages {
            headers.insert("X-Tika-OCRLanguage", languages.parse().unwrap());
        }
        headers
    }

    /// Returns text contained in the file given as arguement
    ///
    /// This is a binding for TIKA API.
    /// You can specify the path of your choice, the list of supported format can be found on
    /// [official website](https://tika.apache.org/2.1.0/formats.html)
    ///
    /// # Examples
    /// ```rust
    /// let tika = tikars::Tika;
    /// let result = tika.get_text("files/lorem.png");
    /// assert!(result.is_ok(), "Got error, check that tika server is running: {}", result.unwrap_err());
    /// print!("{}", result.unwrap());
    /// ```
    /// # Errors
    pub fn get_text(&self, file_path: &str) -> Result<String, Error> {
        let file_content = std::fs::read(file_path)?;
        let client = reqwest::blocking::Client::new();

        let resp = client
            .put(&self.url)
            .headers(self.get_headers(file_path))
            .body(file_content)
            .send()?
            .text()?;
        Ok(resp)
    }
}
